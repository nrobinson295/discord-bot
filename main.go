package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/nrobinson295/discord-bot/common"
	"gitlab.com/nrobinson295/discord-bot/models"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//var a map[string]func(s *discordgo.Session, i *discordgo.InteractionCreate)
var (
	GuildID = common.Env("GUILDID")

	BuildNames = map[int64]string{
		1: "Tank",
		2: "Mage",
		3: "Healer",
		4: "Ranged dps",
		5: "Brawler dps",
	}

	commands = []*discordgo.ApplicationCommand{
		{
			Name: "basic-command",
			// All commands and options must have a description
			// Commands/options without description will fail the registration
			// of the command.
			Description: "Basic Command",
		},
		{
			Name:        "war",
			Description: "Player information for war registry",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "ign",
					Description: "Ingame Name",
					Required:    true,
				},
				{
					Type:        discordgo.ApplicationCommandOptionInteger,
					Name:        "level",
					Description: "Character Level",
					Required:    true,
				},
				{
					Name:        "build",
					Description: "Pick your build",
					Type:        discordgo.ApplicationCommandOptionInteger,
					Choices: []*discordgo.ApplicationCommandOptionChoice{
						{
							Name:  "tank",
							Value: 1,
						},
						{
							Name:  "mage",
							Value: 2,
						},
						{
							Name:  "healer",
							Value: 3,
						},
						{
							Name:  "ranged-dps",
							Value: 4,
						},
						{
							Name:  "brawler-dps",
							Value: 5,
						},
					},
					Required: true,
				},
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "weapon-mastery",
					Description: "Please input your weapon levels you will use in the war",
					Required:    true,
				},
			},
		},
	}
	commandHandlers = map[string]func(s *discordgo.Session, i *discordgo.InteractionCreate){
		"war": func(s *discordgo.Session, i *discordgo.InteractionCreate) {
			// get user war id
			war, err := models.GetWar(i.Member.User.ID)
			//handle error
			if err != nil {
				log.Println(err)
			}
			//if no user war info then create new
			if war == nil {
				war = &models.WarInfo{
					ID: primitive.NewObjectID(),
				}
			}

			war.UserId = i.Member.User.ID
			war.Ign = i.ApplicationCommandData().Options[0].StringValue()
			war.Level = i.ApplicationCommandData().Options[1].IntValue()
			war.Build = BuildNames[i.ApplicationCommandData().Options[2].IntValue()]
			war.Mastery = i.ApplicationCommandData().Options[3].StringValue()

			_, err = models.UpsertWar(war)
			if err != nil {
				log.Println(err)
			}
			/*msgformat :=
				`Player war information:
			> IGN: %s
			> Level: %d
			> Build: %v
			> Mastery: %s
			`*/

			s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: "",
				},
			})

			s.ChannelMessageSendComplex(i.ChannelID, &discordgo.MessageSend{Embed: &discordgo.MessageEmbed{

				Title:     "Player Information",
				Type:      discordgo.EmbedTypeRich,
				Color:     war.GetColor(),
				Timestamp: time.Now().Format(time.RFC3339),
				Fields: []*discordgo.MessageEmbedField{
					{
						Name:   "IGN:",
						Value:  war.Ign,
						Inline: true,
					},
					{
						Name:   "Level:",
						Value:  fmt.Sprintf("%d", war.Level),
						Inline: true,
					},
					{
						Name:   "Build",
						Value:  war.Build,
						Inline: true,
					},
					{
						Name:  "Mastery",
						Value: war.Mastery,
					},
				},
			}})

		},
	}
)

func main() {
	discord, err := discordgo.New("Bot " + common.Env("BOT_TOKEN"))
	if err != nil {
		panic(err)
	}
	discord.AddHandler(func(s *discordgo.Session, i *discordgo.InteractionCreate) {
		if h, ok := commandHandlers[i.ApplicationCommandData().Name]; ok {
			h(s, i)
		}
	})
	discord.AddHandler(func(s *discordgo.Session, r *discordgo.Ready) {
		fmt.Println("Bot is up!")
	})
	err = discord.Open()
	if err != nil {
		log.Fatalf("Cannot open the session: %v", err)
	}

	for _, v := range commands {
		_, err := discord.ApplicationCommandCreate(discord.State.User.ID, GuildID, v)
		if err != nil {
			log.Panicf("Cannot create '%v' command: %v", v.Name, err)
		}
	}

	defer discord.Close()
	closeDb := common.StartMongo()
	defer closeDb()
	stop := make(chan os.Signal)
	signal.Notify(stop, os.Interrupt)
	<-stop
	log.Println("Gracefully shutdowning")

}
