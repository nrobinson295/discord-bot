package common

import "os"

func Env(name string) string {
	result, ok := os.LookupEnv(name)
	if !ok {
		panic("Missing " + name + " environment variable")
	}
	return result
}
