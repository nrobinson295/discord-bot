package common

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	DATABASE_CONNECTION   = Env("DATABASE_CONNECTION")
	Mongo                 = &mongo.Client{}
	DefaultReplaceOptions = options.FindOneAndReplace().SetUpsert(true).SetReturnDocument(options.After)
)

func StartMongo() context.CancelFunc {
	var err error
	Mongo, err = mongo.NewClient(options.Client().ApplyURI(DATABASE_CONNECTION))
	if err != nil {
		panic(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	err = Mongo.Connect(ctx)
	if err != nil {
		panic(err)
	}
	println("Mongo connected")
	return cancel
}
