module gitlab.com/nrobinson295/discord-bot

go 1.16

require (
	github.com/bwmarrin/discordgo v0.23.3-0.20210730152208-ab47f123ba40
	go.mongodb.org/mongo-driver v1.7.1
)
