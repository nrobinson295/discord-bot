package models

import (
	"context"

	"gitlab.com/nrobinson295/discord-bot/common"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func WarCollection() *mongo.Collection {
	return common.Mongo.Database("warbot").Collection("war")
}

type WarInfo struct {
	UserId  string             `bson:"userid"`
	ID      primitive.ObjectID `bson:"_id"`
	Ign     string             `bson:"ign"`
	Level   int64              `bson:"level"`
	Build   string             `bson:"build"`
	Mastery string             `bson:"mastery"`
}

func (w *WarInfo) GetColor() int {
	if w.Build == "Tank" {
		return 0x33ff00
	}
	if w.Build == "Mage" {
		return 0x9000ff
	}
	if w.Build == "Healer" {
		return 0
	}
	if w.Build == "Ranged dps" {
		return 0
	}
	if w.Build == "Brawler dps" {
		return 0
	}
	return 0
}

func GetWar(userid string) (*WarInfo, error) {
	warinfo := &WarInfo{}
	err := WarCollection().FindOne(context.Background(), bson.M{"userid": userid}).Decode(warinfo)
	return warinfo, err
}

func UpsertWar(warinfo *WarInfo) (*WarInfo, error) {
	err := WarCollection().FindOneAndReplace(context.Background(), bson.M{"userid": warinfo.UserId}, warinfo, common.DefaultReplaceOptions).Decode(warinfo)
	return warinfo, err
}
